package main

import "github.com/gorilla/mux"

func mapRoutes(r *mux.Router) {
	r.HandleFunc("/", hello)
	r.HandleFunc("/authorization", authorization)
	r.HandleFunc("/token", token)
	r.HandleFunc("/login", login)
}
