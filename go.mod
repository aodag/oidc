module oidc

go 1.13

require (
	github.com/go-sqlite/sqlite3 v0.0.0-20180313105335-53dd8e640ee7
	github.com/gonuts/binary v0.2.0 // indirect
	github.com/gorilla/mux v1.7.3
	github.com/jmoiron/sqlx v1.2.0
	github.com/mattn/go-sqlite3 v1.11.0
	github.com/stretchr/testify v1.4.0
	golang.org/x/text v0.3.2 // indirect
)
