package main

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"oidc/oauth2"
	"strings"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

var (
	oauth2Service = oauth2.Service{
		DB: sqlx.MustOpen("sqlite3", ":memory:"),
	}
)

func hello(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "Hello, world!")
}

func createURL(base string, values map[string]string) string {
	urlValues := url.Values{}
	for k, v := range values {
		urlValues.Set(k, v)
	}
	return base + "?" + urlValues.Encode()
}

func oauthError(w http.ResponseWriter, redirectURI, msg string, err error) {
	w.WriteHeader(http.StatusTemporaryRedirect)
	values := map[string]string{
		"error": msg,
	}
	if err != nil {
		values["error_description"] = err.Error()
	}
	location := createURL(redirectURI, values)
	w.Header().Set("Location", location)
}

func parseScopes(scopes string) []string {
	parsed := []string{}
	splited := strings.Split(scopes, ",")
	for i := range splited {
		trimed := strings.Trim(splited[i], " ")
		parsed = append(parsed, trimed)
	}
	return parsed
}

func authorization(w http.ResponseWriter, req *http.Request) {
	values := req.URL.Query()
	clientID := values.Get("client_id")
	client, err := oauth2Service.GetClient(req.Context(), clientID)
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}
	if client == nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, "unauthorized_client")
		return
	}
	redirectURI := values.Get("redirect_uri")
	if !client.ValidateRedirectURI(redirectURI) {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, "invalid_request")
		return
	}
	responseType := values.Get("response_type")
	if responseType != "code" {
		oauthError(w, redirectURI, "unsupported_response_type", nil)
		return
	}
	scopes := parseScopes(values.Get("scope"))
	if err := client.ValidateScopes(scopes); err != nil {
		log.Print(err)
		oauthError(w, redirectURI, "invalid_scope", err)
		return
	}
	state := values.Get("state")
	w.WriteHeader(http.StatusTemporaryRedirect)
	location := createURL(redirectURI, map[string]string{
		"code":  "testing-code",
		"state": state,
	})
	w.Header().Set("Location", location)
}

func token(w http.ResponseWriter, req *http.Request) {

}

func login(w http.ResponseWriter, req *http.Request) {

}
