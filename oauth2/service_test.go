package oauth2

import (
	"context"
	"testing"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"github.com/stretchr/testify/suite"
)

type ServiceTestSuite struct {
	suite.Suite
	target *Service
	db     *sqlx.DB
	ctx    context.Context
}

func (suite *ServiceTestSuite) SetupTest() {
	suite.target = &Service{
		DB: sqlx.MustOpen("sqlite3", ":memory:"),
	}
	suite.db = suite.target.DB
	suite.db.MustExec(`CREATE TABLE oauth2_users (id integer primary key, user_name varchar(255), password_digest varchar(255))`)
	suite.ctx = context.Background()
}

func (suite ServiceTestSuite) TestAuthenticate() {
	user := &User{
		Name: "testing-user",
	}
	suite.NoError(user.SetPassword("secret"))
	suite.NoError(suite.target.AddUser(suite.ctx, user))
	authenticatedUser, err := suite.target.Authenticate(suite.ctx, "testing-user", "secret")
	suite.NoError(err)
	suite.NotNil(authenticatedUser)
	suite.Equal(user.ID, authenticatedUser.ID)
}

func TestServiceTestSuite(t *testing.T) {
	suite.Run(t, &ServiceTestSuite{})
}
