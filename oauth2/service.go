package oauth2

import (
	"context"
	"fmt"
	"log"

	"github.com/jmoiron/sqlx"
)

//Service provides oauth2 service
type Service struct {
	DB *sqlx.DB
}

//GetClient returns Client specified clientID
func (s *Service) GetClient(ctx context.Context, clientID string) (*Client, error) {
	client, err := s.queryClient(ctx, s.DB, clientID)
	if err != nil {
		return nil, err
	}
	scopes, err := s.queryScopes(ctx, s.DB, clientID)
	if err != nil {
		return nil, err
	}
	redirectURIs, err := s.queryRedirectURIs(ctx, s.DB, clientID)
	if err != nil {
		return nil, err
	}
	client.Scopes = scopes
	client.RedirectURIs = redirectURIs
	return client, nil
}

func (s *Service) queryClient(ctx context.Context, q sqlx.QueryerContext, clientID string) (*Client, error) {
	var client Client
	err := q.QueryRowxContext(ctx, "SELECT client_id FROM oauth2_clients WHERE client_id = ?", clientID).StructScan(&client)
	if err != nil {
		return nil, err
	}
	return &client, nil
}

func (s *Service) queryRedirectURIs(ctx context.Context, q sqlx.QueryerContext, clientID string) ([]string, error) {
	rows, err := q.QueryContext(ctx, "SELECT redirect_uri FROM oauth2_redirect_uris WHERE client_id = ?", clientID)
	if err != nil {
		return nil, err
	}
	uris := []string{}
	for rows.Next() {
		var uri string
		err := rows.Scan(&uri)
		if err != nil {
			return nil, err
		}
		uris = append(uris, uri)
	}
	return uris, nil
}

func (s Service) queryScopes(ctx context.Context, q sqlx.QueryerContext, clientID string) ([]string, error) {
	rows, err := q.QueryContext(ctx, "SELECT scope FROM oauth2_scopes WHERE client_id = ?", clientID)
	if err != nil {
		return nil, err
	}
	scopes := []string{}
	for rows.Next() {
		var scope string
		err := rows.Scan(&scope)
		if err != nil {
			return nil, err
		}
		scopes = append(scopes, scope)
	}
	return scopes, nil
}

//Authenticate user
func (s Service) Authenticate(ctx context.Context, username, password string) (*User, error) {
	user, err := s.GetUser(ctx, username)
	if err != nil {
		return nil, err
	}
	if user == nil {
		return nil, fmt.Errorf("invalid username")
	}
	success, err := user.VerifyPassword(password)
	if err != nil {
		return nil, err
	}
	if !success {
		return nil, fmt.Errorf("invalid password")
	}
	return user, nil
}

//GetUser get user specified username
func (s Service) GetUser(ctx context.Context, username string) (*User, error) {
	user, err := s.queryUser(ctx, s.DB, username)
	return user, err
}

func (s *Service) queryUser(ctx context.Context, q sqlx.QueryerContext, username string) (*User, error) {
	user := &User{}
	row := q.QueryRowxContext(ctx, "SELECT id, user_name, password_digest FROM oauth2_users WHERE user_name = ?", username)
	err := row.StructScan(user)
	if err != nil {
		return nil, err
	}
	return user, nil
}

//AddUser adds user and sync id
func (s *Service) AddUser(ctx context.Context, user *User) error {
	tx, err := s.DB.Beginx()
	defer tx.Rollback()
	if err != nil {
		return err
	}
	id, err := s.insertUser(ctx, tx, *user)
	if err != nil {
		return err
	}
	user.ID = id
	tx.Commit()
	return nil
}

func (s *Service) insertUser(ctx context.Context, e sqlx.ExecerContext, user User) (int64, error) {
	r, err := e.ExecContext(ctx, "INSERT INTO oauth2_users (user_name, password_digest) VALUES (?, ?)", user.Name, user.PasswodDigest)
	if err != nil {
		return 0, err
	}

	id, err := r.LastInsertId()
	if err != nil {
		return 0, err
	}
	log.Printf("create user id = %d", id)
	return id, nil
}
