package oauth2

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDigestPassword(t *testing.T) {
	password := "abcdefg"
	result, err := DigestPassword(password)
	if err != nil {
		t.Fatal(err)
	}
	assert.NotEmpty(t, result)
	assert.Equal(t, MustDigestPassword(password), result)
	assert.NotEqual(t, MustDigestPassword(password+"x"), result)
}
