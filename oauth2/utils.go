package oauth2

import (
	"crypto/sha256"
	"encoding/hex"
)

//DigestPassword creates hashed hex digest
func DigestPassword(password string) (string, error) {
	h := sha256.New()
	return hex.EncodeToString(h.Sum([]byte(password))), nil
}

//MustDigestPassword must create hashed hex digest
func MustDigestPassword(password string) string {
	d, err := DigestPassword(password)
	if err != nil {
		panic(err)
	}
	return d
}
