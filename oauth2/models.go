package oauth2

import (
	"fmt"
)

//Client is oauth2 client.
type Client struct {
	ClientID     string `db:"client_id"`
	RedirectURIs []string
	Scopes       []string
}

//ValidateRedirectURI checks that c has RedirectURI
func (c Client) ValidateRedirectURI(redirectURI string) bool {
	for i := range c.RedirectURIs {
		r := c.RedirectURIs[i]
		if r == redirectURI {
			return true
		}
	}
	return false
}

//ValidateScopes checks that c has all scope
func (c Client) ValidateScopes(scopes []string) error {
	for i := range scopes {
		scope := scopes[i]
		if !c.ValidateScope(scope) {
			return fmt.Errorf("%s is invalid scope", scope)
		}
	}
	return nil
}

//ValidateScope checks that c has scope
func (c Client) ValidateScope(scope string) bool {
	for i := range c.Scopes {
		s := c.Scopes[i]
		if s == scope {
			return true
		}
	}
	return false
}

//User is authenticatable user
type User struct {
	ID            int64  `db:"id"`
	Name          string `db:"user_name"`
	PasswodDigest string `db:"password_digest"`
}

//SetPassword modify PasswordDigest to digested password
func (u *User) SetPassword(password string) error {
	d, err := DigestPassword(password)
	if err != nil {
		return err
	}
	u.PasswodDigest = d
	return nil
}

//VerifyPassword check password is valid
func (u User) VerifyPassword(password string) (bool, error) {
	d, err := DigestPassword(password)
	if err != nil {
		return false, err
	}
	return u.PasswodDigest == d, nil
}
