package oauth2

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type UserTestSuite struct {
	suite.Suite
}

func (suite UserTestSuite) TestPassword() {
	u := User{}
	password := "this-is-password"
	err := u.SetPassword(password)
	suite.NoError(err)
	result, err := u.VerifyPassword(password)
	suite.NoError(err)
	suite.True(result)
}

func TestUserTestSuite(t *testing.T) {
	suite.Run(t, new(UserTestSuite))
}
