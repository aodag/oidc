package main

import (
	"context"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

func TestHello(t *testing.T) {
	req, err := http.NewRequest(http.MethodGet, "/", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(hello)
	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	assert.Equal(t, "Hello, world!", rr.Body.String())
}

type AuthorizationTestSuite struct {
	suite.Suite
	db  *sqlx.DB
	ctx context.Context
}

func (suite *AuthorizationTestSuite) SetupTest() {
	suite.ctx = context.Background()
	suite.db = oauth2Service.DB

	suite.db.MustExec(`CREATE TABLE oauth2_clients (client_id varchar(255) primary key)`)
	suite.db.MustExec(`CREATE TABLE oauth2_scopes (client_id varchar(255), scope varchar(255))`)
	suite.db.MustExec(`CREATE TABLE oauth2_redirect_uris (client_id varchar(255), redirect_uri varchar(255))`)
	suite.db.MustExec(`INSERT INTO oauth2_clients (client_id) VALUES ('testing')`)
	suite.db.MustExec(`INSERT INTO oauth2_redirect_uris (client_id, redirect_uri) VALUES ('testing', 'https://example.com/oauth')`)
	suite.db.MustExec(`INSERT INTO oauth2_scopes (client_id, scope) VALUES ('testing', 'email')`)
	suite.db.MustExec(`INSERT INTO oauth2_scopes (client_id, scope) VALUES ('testing', 'openid')`)

}

func (suite *AuthorizationTestSuite) TestAuthorization() {

	req, err := http.NewRequest(http.MethodGet, "/authorization?client_id=testing&state=abc&scope=openid,email&response_type=code&redirect_uri=https://example.com/oauth", nil)
	suite.NoError(err)
	req = req.WithContext(suite.ctx)
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(authorization)
	handler.ServeHTTP(rr, req)

	suite.Equal(http.StatusTemporaryRedirect, rr.Code)
	suite.Empty(rr.Body.String())
	location := rr.Header().Get("Location")
	u, err := url.Parse(location)
	suite.NoError(err)
	q := u.Query()
	suite.Empty(q.Get("error"))
	suite.Empty(q.Get("error_description"))
	suite.Equal("abc", q.Get("state"))
	suite.Equal("testing-code", q.Get("code"))
}

func TestAuthorizationTestSuite(t *testing.T) {
	suite.Run(t, &AuthorizationTestSuite{})
}

func TestToken(t *testing.T) {
	req, err := http.NewRequest(http.MethodGet, "/token", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(token)
	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
}

func TestLogin(t *testing.T) {
	req, err := http.NewRequest(http.MethodGet, "/login", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(login)
	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
}
