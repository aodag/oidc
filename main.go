package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()
	mapRoutes(r)
	http.Handle("/", r)
	http.ListenAndServe(":8080", nil)
}
